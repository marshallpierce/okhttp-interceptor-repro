package org.mpierce.okhttp.interceptor

import io.ktor.application.call
import io.ktor.application.install
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.server.engine.ApplicationEngine
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import retrofit2.Retrofit
import retrofit2.http.GET
import java.io.IOException
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit


internal class InterceptorTest {
    private lateinit var server: ApplicationEngine

    private val port = 12000

    private val client = OkHttpClient.Builder()
            .addInterceptor(ThrowingInterceptor())
            .callTimeout(1000, TimeUnit.MILLISECONDS)
            .build()

    private val service = Retrofit.Builder()
            .baseUrl("http://localhost:$port")
            .client(client)
            .build()
            .create(DummyServiceClient::class.java)

    @BeforeEach
    internal fun setUp() {
        server = embeddedServer(Netty, port = port) {
            install(Routing) {
                get("/ok") {
                    call.respond(HttpStatusCode.OK)
                }
                get("/sad") {
                    call.respond(HttpStatusCode.InternalServerError)
                }
            }
        }
        server.start(wait = false)
    }

    @AfterEach
    internal fun tearDown() {
        server.stop(10, 10, TimeUnit.MILLISECONDS)
    }

    @Test
    internal fun clientCallWorksNormallyWithSuccessfulResponse() {
        val call = client.newCall(Request.Builder().url("http://localhost:$port/ok").build())

        val resp = call.asCompletableFuture().get(1000, TimeUnit.MILLISECONDS)
        assertEquals(200, resp.code)
    }

    @Test
    internal fun clientNeverCallsCallbackWhenInterceptorThrowsUnchecked() {
        val call = client.newCall(Request.Builder().url("http://localhost:$port/sad").build())

        // times out
        call.asCompletableFuture().get(1000, TimeUnit.MILLISECONDS)
    }

    @Test
    internal fun retrofitServiceDoesntCompleteFutureWhenInterceptorThrowsUnchecked() {

        // times out
        service.sad().get(1000, TimeUnit.MILLISECONDS)
    }
}

private fun Call.asCompletableFuture(): CompletableFuture<Response> {
    val future = CompletableFuture<Response>()
    enqueue(object : Callback {
        override fun onFailure(call: Call, e: IOException) {
            future.completeExceptionally(e)
        }

        override fun onResponse(call: Call, response: Response) {
            future.complete(response)
        }
    })

    return future
}

class ThrowingInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val resp = chain.proceed(chain.request())

        if (resp.code == 500) {
            throw RuntimeException("kaboom")
        }

        return resp
    }
}

interface DummyServiceClient {

    @GET("/sad")
    fun sad(): CompletableFuture<Unit>

}
