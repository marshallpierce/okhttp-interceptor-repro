plugins {
    kotlin("jvm") version "1.3.41"
    id("com.github.ben-manes.versions") version "0.21.0"
}

repositories {
    jcenter()
}

val deps by extra {
    mapOf(
            "junit" to "5.5.0",
            "ktor" to "1.2.2"
    )
}

dependencies {
    implementation(kotlin("stdlib"))

    testImplementation("com.squareup.okhttp3:okhttp:4.0.0")
    testImplementation("com.squareup.retrofit2:retrofit:2.6.0")

    testImplementation("io.ktor:ktor-server-core:${deps["ktor"]}")
    testImplementation("io.ktor:ktor-server-netty:${deps["ktor"]}")
    testImplementation("io.ktor:ktor-jackson:${deps["ktor"]}")
    testRuntime("org.slf4j:slf4j-simple:1.7.26")

    testImplementation("org.junit.jupiter:junit-jupiter-api:${deps["junit"]}")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:${deps["junit"]}")
}

tasks {
    test {
        useJUnitPlatform()
    }
}
